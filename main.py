import json

from utils.vk_api import VkApi
from utils.vk_assist import VkAssist
from utils.vk_parser import VkParser

from libs.logger import logger

if __name__ == '__main__':
    # True
    vk_parser_functions_excecute = {
        'get_user_mentions' : False,
        'get_friends' : False
    }
    vk_api_functions_excecute = {
        'get_info_from_vk_page' : False,
        'get_friends' : False,
        'get_top_groups' : False,
        'get_estimate_age' : False,
        'get_common_friends' : False,
        'get_hidden_friends' : False,
        'get_user_age' : False
    }
    vk_assist_functions_excecute = {
        'get_user_info_by_nickname_regvk' : True
    }
    
    vk_parser = None
    if any(vk_parser_functions_excecute.values()):
        vk_parser = VkParser(login='+79964599337', passwd='Qwerty6/')

    if vk_parser is not None:
        if vk_parser_functions_excecute['get_user_mentions']:
            print(vk_parser.get_user_mentions(92914))
        if vk_parser_functions_excecute['get_friends']:
            print(vk_parser.get_friends(21531829))
    
    if vk_api_functions_excecute['get_info_from_vk_page']:
        print(VkApi.get_info_from_vk_page('lucky_lexus'))
    if vk_api_functions_excecute['get_friends']:
        print(VkApi.get_friends('lucky_lexus'))
    if vk_api_functions_excecute['get_top_groups']:
        print(VkApi.get_top_groups('lucky_lexus'))
    if vk_api_functions_excecute['get_estimate_age']:
        print(VkApi.get_estimate_age('lucky_lexus'))
    if vk_api_functions_excecute['get_common_friends']:
        print(VkApi.get_common_friends(['21531829', '137317727', 'id7086336', 'lucky_lexus']))
    if vk_api_functions_excecute['get_hidden_friends']:
        print(VkApi.get_hidden_friends(user_name='nzavadskiy', time=True))
    if vk_api_functions_excecute['get_user_age']:
        print(VkApi.get_user_age('lucky_lexus'))

    if vk_assist_functions_excecute['get_user_info_by_nickname_regvk']:
        print(VkAssist.get_user_info_by_nickname_regvk('lucky_lexus'))