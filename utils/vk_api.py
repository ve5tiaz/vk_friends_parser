from datetime import datetime
from itertools import zip_longest
from datetime import datetime
from dateutil.relativedelta import relativedelta

from libs.requests import Request
from libs.enums import VkMethod
from libs.logger import logger


class VkApi:
    """
    Класс получения данных из ВКонтакте с использованием API
    """

    access_token = "42e8570d10b35b88d8f378360906d212d8f5c78a33cd3875a968ce4cbc0a057ff3c85eaf2c8f0c5d11706"
    version = "5.126"
    api_url = "https://api.vk.com/method"

    @classmethod
    def normalize_user_name(cls, user_name) -> int:
        """
        Функция приведения идентификатора пользователя к числовому виду
        :param user_name: идентификатор пользователя ВК
        :return:
        """
        if type(user_name) is int:
            return user_name
        elif type(user_name) is str:
            if user_name.isdigit():
                return int(user_name)
            elif 'id' == user_name[:2] and user_name[2:].isdigit():
                return int(user_name[2:])
            else:
                return int(cls.get_user_id_by_nickname(user_name))

    @classmethod
    def get_user_id_by_nickname(cls, nickname: str):
        """
        Функция для получения id пользователя ВК от его никнейма через API Вконтакте
        :param nickname: никнейм пользователя ВК
        :return:
        """
        info = cls.get_info(vk_method=VkMethod.user_get_name, user_ids=nickname)
        if info.get('id', None) is not None:
            return info['id']

    @classmethod
    def get_info_from_vk_page(cls, user_name: str):
        """
        Функция для получения информации о пользователе с помощью API запроса
        :param user_name: id или никнейм пользователя в ВК
        :return response: ответ API Вконтакте (cтрока в формате JSON)
        """
        return cls.get_info(vk_method=VkMethod.user_get_name,
                                user_ids=user_name, fields=VkMethod.user_get_fields.value)
        
    @classmethod
    def get_info(cls, vk_method: VkMethod, **kwargs):
        """
        Функция для получения информации по API ВКонтакте
        :param 
            vk_method: метод API Вконтакте, по которому будет загружаться информация
        :return:
        """
        kwargs.update({"access_token": cls.access_token, "v": cls.version})
        response = Request.send_api_request(url=f'{cls.api_url}/{vk_method.value}', params=kwargs)
        if response:
            if response.get('response', None):
                if vk_method in [VkMethod.groups_is_member, VkMethod.friends_get_name,
                                    VkMethod.groups_get_name, VkMethod.user_search_name]:
                    return response['response']
                else:
                    return response['response'][0]
            elif response.get('error', None):
                if response['error'].get('error_msg', None):
                    logger.warning('%s, ВК метод: %s, параметры: %s',
                                        response['error']['error_msg'],
                                        vk_method.value, 
                                        str(kwargs))
                else:
                    logger.warning('Неизвестная ошибка, ВК метод: %s, параметры: %s',
                                        vk_method.value, 
                                        str(kwargs))
                return None  
    
    @classmethod
    def get_friends(cls, user_name, **kwargs):
        """
        Функция для получения друзей пользователя ВКонтакте
        :param 
            user_name: имя или id пользователя
        :return:
        """
        normalized_user_name = cls.normalize_user_name(user_name)
        if kwargs.get('fields', None):
            info = cls.get_info(vk_method=VkMethod.friends_get_name,
                                    user_id=normalized_user_name,
                                    fields=kwargs['fields'])
        else:
            info = cls.get_info(vk_method=VkMethod.friends_get_name, user_id=normalized_user_name)

        if info:
            if info.get('items', None) is not None:
                return info['items']

    @classmethod
    def get_groups(cls, user_name: str):
        """
        Функция для получения групп пользователя ВКонтакте
        :param 
            user_name: имя или id пользователя
        :return:
        """
        normalized_user_name = cls.normalize_user_name(user_name)
        info = cls.get_info(vk_method=VkMethod.groups_get_name, user_id=normalized_user_name)
        if info:
            if info.get('items', None) is not None:
                return info['items']

    @classmethod
    def get_estimate_age(cls, user_name: str):
        """
        Функция для получения среднего возраста пользователя
        :param user_name: id или никнейм пользователя
        :return список идентификаторов групп:
        """
        result = {}
        now_year = int(datetime.now().year)
        list_info_about_friends = cls.get_friends(user_name=user_name, fields='bdate')
        if list_info_about_friends:
            for friend in list_info_about_friends:
                if (friend.get('bdate', None)) is None:
                    continue
                else:
                    if friend['bdate'].count('.') == 2:
                        year_born = int(friend['bdate'].split('.')[2])
                        if now_year > year_born:
                            if result.get(year_born, None) is None:
                                result[year_born] = 1
                            else:
                                result[year_born] += 1

            list_ages_friends = sorted(result.items(), key=lambda pair: (-pair[1], pair[0]))

            if not list_ages_friends:
                return

            if len(list_ages_friends) == 1 or len(list_ages_friends) == 2:
                return now_year - list_ages_friends[0][0]
            else:
                if abs(list_ages_friends[0][0] - list_ages_friends[1][0]) == 1 and abs(list_ages_friends[0][0] - list_ages_friends[2][0]) == 1:
                    return now_year - list_ages_friends[0][0]
                else:
                    return now_year - int(sum([item[0] for index, item in enumerate(list_ages_friends) if index <= 3])/4)

    @classmethod
    def get_top_groups(cls, user_name: str):
        """
        Функция для получения интересов пользователя по группам Вконтакте
        :param user_name: id или никнейм пользователя
        :return список идентификаторов групп:
        """
        normalized_user_name = cls.normalize_user_name(user_name)
        user_groups = cls.get_groups(normalized_user_name)
        user_friends = cls.get_friends(normalized_user_name)

        groups_members_count = dict.fromkeys(user_groups, 1)

        for user_group in user_groups:
            for group_of_users in zip_longest(*[iter(user_friends)] * 100):
                group_of_users = [str(x) for x in group_of_users if x is not None]
                is_member_list = cls.get_info(user_ids=','.join(group_of_users),
                                              group_id=str(user_group),
                                              vk_method=VkMethod.groups_is_member)
                if is_member_list:
                    member_list = [x for x in is_member_list if x['member'] == 1]
                    groups_members_count[user_group] += len(member_list)
        
        list_friends_groups_count = sorted(groups_members_count.items(), key=lambda item: item[1], reverse=True)
                
        return list_friends_groups_count[:5]

    @classmethod
    def get_common_friends(cls, users: list):
        """
        Функция для получения общих друзей
        :param users: список пользователей
        :return список общих друзей
        """
        if type(users) is not list:
            logger.warning("""Параметр - список, переданные параметры: %s""", str(users))
            return None

        if len(users) < 2:
            logger.warning("""Необходимо указать минимум двух пользователей в 
            функции получения общих друзей, переданные параметры: %s""", str(users))
            return None

        users_friends = []

        for user in users:
            user_friends = cls.get_friends(user)
            if user_friends:
                users_friends.append(set(user_friends))

        return [users_friends[0].intersection(*users_friends)]

    @classmethod
    def get_hidden_friends(cls, user_name: str, time: bool):
        """
        Функция для получения скрытых друзей пользователя ВКонтакте
        :param 
            user_name: пользователь
            time: необходимость подсчета примерного времени выполнения
        :return список скрытых друзей
        """
        if time:
            print('Анализ предположительного времени выполнения..')
        time_1_user_min = 0.41 / 60

        normalized_user_name = cls.normalize_user_name(user_name)
        user_friends = cls.get_friends(user_name=normalized_user_name)

        if user_friends:

            user_friends_friends = set()
            for user_friend in user_friends:
                friends = cls.get_friends(user_name=user_friend)
                if friends:
                    user_friends_friends.update(set(friends))
            
            if time:
                print('Примерное время до окончания: {:.2f} минут'.format(len(user_friends_friends) * time_1_user_min) )
            
            hidden_friends = set()
            for user in user_friends_friends:
                friends = cls.get_friends(user_name=user)
                if friends:
                    if normalized_user_name in friends and user not in user_friends:
                        print(user)
                        hidden_friends.add(user)
            
            return [hidden_friends]
        else:
            return None

    @classmethod
    def get_user_full_name(cls, user_name: str):
        """
        Функция получения полного имени пользователя
        :param
            user_name: имя пользователя
        :return
        """
        user_info = cls.get_info(vk_method=VkMethod.user_get_name, user_ids=user_name)
        return '{0} {1}'.format(user_info.get('first_name', ''), user_info.get('last_name', '')).strip()

    @classmethod
    def check_field_exists_in_vk_response(cls, response, field: str) -> bool:
        """
        Функция проверки существования поля в ответе от ВКонтакте
        :param
            response: ответ
            field: имя поля
        :return
        """
        if response:
            if response.get(field, None):
                return True        
        return False
    
    @classmethod
    def check_user_id_exists_in_response_items(cls, response, user_id: int) -> bool:
        """
        Функция проверки того, что id пользователя присутствует в поисковой выдаче
        :param
            response: результат поиска
            user_id: id пользователя
        :return
        """
        if cls.check_field_exists_in_vk_response(response=response, field='items'):
            if user_id in [item['id'] for item in response['items']]:
                return True
            else:
                return False

    @classmethod
    def search_users_with_offset(cls, **kwargs):
        """
        Функция поиска пользователей ВКонтакте с использованием смещения (если возвращается значений больше 1000)
        :param
        :return список объетов пользователей
        """
        users_info_for_count = cls.get_info(vk_method=VkMethod.user_search_name, count=1, **kwargs)
        users_count = 0
        if cls.check_field_exists_in_vk_response(response=users_info_for_count, field='count'):
            users_count = users_info_for_count['count']
        
        users_items = dict.fromkeys(['items'], [])
        for offset_coeff in range(0, users_count//1000 + 1):
            users_info = cls.get_info(vk_method=VkMethod.user_search_name, count=1000, offset=offset_coeff*1000, **kwargs)
            if cls.check_field_exists_in_vk_response(response=users_info, field='items'):
                users_items['items'].extend(users_info['items'])

        return users_items

    @classmethod
    def search_user_age(cls, q: str, age_from: int, age_to: int, user_id: int):
        """
        Функция поиска возраста пользователя ВКонтакте по полному имени
        :param
            q: полное имя пользователя
            age_from: левая граница диапазона возраста
            age_to: правая граница диапазона возраста
            user_id: id пользователя
        :return
        """
        age_2 = age_to - ((age_to - age_from) // 2) - 1
        users_1 = cls.search_users_with_offset(q=q, age_from=age_from, age_to=age_2)        
        if cls.check_user_id_exists_in_response_items(response=users_1, user_id=user_id):
            if age_from == age_2:
                return age_from
            else:
                return cls.search_user_age(q=q, age_from=age_from, age_to=age_2, user_id=user_id)
        else:
            users_2 = cls.search_users_with_offset( q=q, age_from=age_2 + 1, age_to=age_to)
            if cls.check_user_id_exists_in_response_items(response=users_2, user_id=user_id):
                if age_2 + 1 == age_to:
                    return age_to
                else:
                    return cls.search_user_age(q=q, age_from=age_2 + 1, age_to=age_to, user_id=user_id)
            else:
                return None
        
    @classmethod
    def get_user_age(cls, user_name: str):
        """
        Функция для получения возраста пользователя ВКонтакте
        :param user_name: пользователь
        :return
        """
        user_info = cls.get_info(vk_method=VkMethod.user_get_name, user_ids=user_name, fields='bdate')        
        if user_info:
            if user_info.get('bdate', None):
                try:
                    return relativedelta(datetime.today(), datetime.strptime(user_info['bdate'], '%d.%m.%Y')).years
                except(ValueError):
                    pass
        else:
            return None
        
        user_full_name = '{0} {1}'.format(user_info.get('first_name', ''), user_info.get('last_name', '')).strip()        
        normalized_user_name = user_info.get('id', None)

        age_from = 1
        age_to = 124

        users = cls.search_users_with_offset(q=user_full_name, age_from=age_from, age_to=age_to)
        if cls.check_user_id_exists_in_response_items(response=users, user_id=normalized_user_name):
            return cls.search_user_age(q=user_full_name, age_from=age_from, age_to=age_to, user_id=normalized_user_name)
