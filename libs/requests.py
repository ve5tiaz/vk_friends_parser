import requests
from requests.exceptions import ConnectionError, ReadTimeout
from json import JSONDecodeError
from libs.logger import logger
from libs.enums import RequestType
from datetime import datetime
from time import sleep


class Request:
    """
    Класс - обертка requests для более гибкой настройки всех запросов
    """



    last_apply_to_api_vk = datetime.now()

    @staticmethod
    def get_new_session():
        return requests.Session()

    @classmethod
    def request(cls, method: str, request_type: RequestType = None, session: requests.Session = None, **kwargs):
        try:
            if kwargs.get('proxies', None) is None:
                kwargs['proxies'] = {}

            if kwargs.get('timeout', None) is None:
                kwargs['timeout'] = 20

            if request_type is RequestType.vk_api_request:
                past_sec_from_last_request = float(((datetime.now() - cls.last_apply_to_api_vk).microseconds / pow(10, 6)))
                if past_sec_from_last_request <= 0.4:
                    sleep(0.4 - past_sec_from_last_request)
                cls.last_apply_to_api_vk = datetime.now()

            if session is None:
                response = requests.request(method, **kwargs)
            else:
                response = session.request(method, **kwargs)

            if response.status_code == 200:
                if request_type is RequestType.vk_api_request:
                    try:
                        return response.json()
                    except JSONDecodeError:
                        logger.info('Ответ не формата JSON, ресурс: %s, параметры: %s', request_type.name, str(kwargs))
                        return response.text
                elif request_type == RequestType.regvk_request:
                    return response.text
                else:
                    return response.text
            else:
                return response.text

        except ConnectionError:
            logger.error('Ошибка соединения, ресурс: %s, параметры: %s', request_type.name, str(kwargs))
            return None
        except ReadTimeout:
            logger.error('Вышло время ожидания ответа, ресурс: %s, параметры: %s', request_type.name, str(kwargs))
            return None

    @classmethod
    def send_api_request(cls, **kwargs):
        return Request.request(method='GET', request_type=RequestType.vk_api_request, **kwargs)

    @classmethod
    def send_php_request(cls, **kwargs):
        return Request.request(method='POST', request_type=RequestType.php_request, **kwargs)

    @classmethod
    def send_regvk_request(cls, **kwargs):
        response = Request.request(method='POST', request_type=RequestType.regvk_request, **kwargs)
        if response:
            return response
