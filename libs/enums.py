from enum import Enum


class RequestType(Enum):
    """
    Класс, описывающий информационные ресурсы, к которым производится обращение
    """
    vk_api_request = 1
    php_request = 2
    regvk_request = 3

class VkMethod(Enum):
    """
    Класс, описывающий методы получения данных из API Вконтакте
    """

    user_get_name = "users.get"
    user_get_fields = ','.join(
            [
                'sex', 'bdate', 'city', 'country', 'home_town', 'photo_max_orig',
                'domain', 'contacts', 'site', 'education', 'universities',
                'schools', 'status', 'last_seen', 'followers_count', 'counters',
                'occupation', 'nickname', 'relatives', 'relation', 'connections',
                'exports', 'about', 'screen_name'
            ]
        )

    user_search_name = "users.search"

    friends_get_name = "friends.get"
    friends_get_mutual = "friends.getMutual"

    groups_get_name = "groups.get"
    groups_get_fields = ''

    groups_is_member = "groups.isMember"

